# Tricky Homes

![Screenshot](screenshot.png)

## Windows

Set up [MSYS2](https://www.msys2.org/) and install the following in a MinGW-w64 Win64 Shell:

```sh
pacman -Syu --needed mingw-w64-x86_64-cmake mingw-w64-x86_64-gcc mingw-w64-x86_64-boost \
mingw-w64-x86_64-openal mingw-w64-x86_64-freetype mingw-w64-x86_64-libvorbis \
mingw-w64-x86_64-libwebp mingw-w64-x86_64-dlfcn mingw-w64-x86_64-libepoxy mingw-w64-x86_64-python3 \
mingw-w64-x86_64-pkg-config
git submodule update --init
mkdir build
cd build
cmake -G"MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=0 ..
make -j$(nproc)
./ggj2019
```
