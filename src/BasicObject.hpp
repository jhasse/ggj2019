#pragma once

/// Die User Data von Box2D wird immer auf einen Zeiger von dieser Klasse gesetzt
class BasicObject {
public:
	virtual bool destroysTreasure() const;
	virtual ~BasicObject() = default;
};
