#include "Block.hpp"

#include "constants.hpp"

#include <box2d/box2d.h>

Block::Block(Type type, b2World& world)
: type(type) {
	auto rollColor = [this]() {
		red = rand() % 0xff + 1;
		green = rand() % 0xff + 1;
		blue = rand() % 0xff + 1;
	};
	do {
		rollColor();
	} while(std::abs(red - 0x3c) + std::abs(green - 0x86) + std::abs(blue - 0xe6) < 70);
	switch (type) {
		case Type::NORMAL:
			sprite = std::make_unique<jngl::Sprite>("gfx/box_normal");
			break;
		case Type::LONG:
			sprite = std::make_unique<jngl::Sprite>("gfx/box_long");
			break;
		case Type::SQUARE:
			sprite = std::make_unique<jngl::Sprite>("gfx/box_square");
			break;
		case Type::TRIANGLE:
			sprite = std::make_unique<jngl::Sprite>("gfx/box_triangle");
			break;
		case Type::BALL:
			sprite = std::make_unique<jngl::Sprite>("gfx/box_round");
			break;
	}
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);

	// Damit der Block erstmal noch nicht fällt, die Gravitation ausschalten:
	body->SetGravityScale(0);
}

void Block::createFixture() {
	b2PolygonShape shape;
	switch (type) {
		case Type::NORMAL:
			shape.SetAsBox(36. / PIXEL_PER_METER / 2., 108. / PIXEL_PER_METER / 2.);
			break;
		case Type::LONG:
			shape.SetAsBox(36. / PIXEL_PER_METER / 2., 157. / PIXEL_PER_METER / 2.);
			break;
		case Type::SQUARE:
			shape.SetAsBox(92. / PIXEL_PER_METER / 2., 92. / PIXEL_PER_METER / 2.);
			break;
		case Type::TRIANGLE: {
			std::vector<b2Vec2> points{
				pixelToMeter({ -48, 48 }),
				pixelToMeter({ -48, -40 }),
				pixelToMeter({ 40, 48 }),
			};
			shape.Set(&points[0], points.size());
			break;
		}
		case Type::BALL:
			b2CircleShape circle;
			circle.m_radius = 50 / PIXEL_PER_METER;
			b2FixtureDef fixtureDef;
			fixtureDef.shape = &circle;
			fixtureDef.density = 4.0f;
			fixtureDef.friction = 1.7f;
			fixtureDef.restitution = 0.9f;
			fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
			fixtureDef.filter.maskBits = 0xffff;
			body->CreateFixture(&fixtureDef);
			body->SetGravityScale(1);
			body->ApplyLinearImpulseToCenter({ 0, -2 * body->GetMass() }, true);
			animation = 0;
			return;
	}
	createFixtureFromShape(shape);
	animation = 0;
}

int Block::getPrice() const {
	if (type == Type::TRIANGLE) {
		return 5;
	}
	if (type == Type::LONG) {
		return 15;
	}
	if (type == Type::BALL) {
		return 40;
	}
	return 10;
}

bool Block::step() {
	animation += (1 - animation) * 0.15;
	return false;
}

void Block::draw(const bool transparent, const float scale) const {
	jngl::pushSpriteAlpha(transparent ? 100 : 255);
	const auto transform = body->GetTransform();
	if (scale > 0.99 && !transparent) {
		jngl::setSpriteColor(red, green, blue);
	}
	sprite->draw(jngl::modelview()
	                 .translate(meterToPixel(transform.p))
	                 .rotate(transform.q.GetAngle())
	                 .scale(animation * scale));
	jngl::setSpriteColor(0xff, 0xff, 0xff, 0xff);
	jngl::popSpriteAlpha();
}
