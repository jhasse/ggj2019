#pragma once

#include "GameObject.hpp"

#include <jngl.hpp>

class b2Body;
class b2World;

class Block : public GameObject {
public:
	enum class Type {
		NORMAL,
		LONG,
		SQUARE,
		TRIANGLE,
		BALL,
	};

	Block(Type, b2World&);
	bool step() override;
	void draw(bool transparent, float scale) const override;

	/// Macht den Block "physisch", also sorgt dafür, dass er in der Welt kollidiert
	void createFixture() override;

	int getPrice() const override;

private:
	const Type type;
	std::unique_ptr<jngl::Sprite> sprite;
	unsigned char red;
	unsigned char green;
	unsigned char blue;

	/// Die Größe in Pixeln
	jngl::Vec2 size;

	float animation = 1;
};
