#include "BuildMenu.hpp"

#include "Block.hpp"
#include "Cannon.hpp"
#include "Cursor.hpp"
#include "Control.hpp"
#include "MyPrecious.hpp"

BuildMenu::BuildMenu(b2World& world, const jngl::Vec2 translate_vec, Cursor* cursor)
: translate_vec(translate_vec), world(world), cursor(cursor) {
	const double xSpacing = 240;
	const double ySpacing = 84;
	double y = -ySpacing * 0.5;
	double text_padding = 30;
	for (int i = 0; i < HEIGHT; ++i) {
		double x = -xSpacing * 1.5 - text_padding;
		for (int j = 0; j < WIDTH; ++j) {
			objects.emplace_back(createObjectAt(j, i));
			objects.back()->setPosition({ x, y });
			x += xSpacing;
		}
		y += ySpacing;
	}
}

BuildMenu::~BuildMenu() = default;

void BuildMenu::step(Control& control) {
	fadeIn += (1 - fadeIn) * 0.05;
	const auto direction = control.getMovement();
	if (!waitForReleaseX and !waitForReleaseY) {
		if (direction.x > 0.5 and direction.y > 0.5) {
			x += 1;
			y += 1;
			waitForReleaseX = waitForReleaseY = true;
		} else if (direction.x < -0.5 and direction.y < -0.5) {
			x -= 1;
			y -= 1;
			waitForReleaseX = waitForReleaseY = true;
		} else if (direction.x > 0.5 and direction.y < -0.5) {
			x += 1;
			y -= 1;
			waitForReleaseX = waitForReleaseY = true;
		} else if (direction.x < -0.5 and direction.y > 0.5) {
			x -= 1;
			y += 1;
			waitForReleaseX = waitForReleaseY = true;
		}
	}
	if (waitForReleaseX) {
		if (std::abs(direction.x) < 0.5) {
			waitForReleaseX = false;
		}
	} else {
		if (direction.x > 0.5) {
			x += 1;
			waitForReleaseX = true;
		} else if (direction.x < -0.5) {
			x -= 1;
			waitForReleaseX = true;
		}
	}
	if (waitForReleaseY) {
		if (std::abs(direction.y) < 0.5) {
			waitForReleaseY = false;
		}
	} else {
		if (direction.y > 0.5) {
			y += 1;
			waitForReleaseY = true;
		} else if (direction.y < -0.5) {
			y -= 1;
			waitForReleaseY = true;
		}
	}
	x = std::clamp(x, 0, WIDTH - 1);
	y = std::clamp(y, 0, HEIGHT - 1);
}

jngl::Vec2 BuildMenu::getSelectedPosition() const {
	return objects.at(x + y * WIDTH)->getPosition() + translate_vec;
}

std::unique_ptr<GameObject> BuildMenu::createObject() const {
	return createObjectAt(x, y);
}

std::unique_ptr<GameObject> BuildMenu::createObjectAt(const int x, const int y) const {
	if (y == 0) {
		auto type = Block::Type::NORMAL;
		if (x == 1) {
			type = Block::Type::LONG;
		}
		if (x == 2) {
			type = Block::Type::SQUARE;
		}
		if (x == 3) {
			type = Block::Type::TRIANGLE;
		}
		float rotation = M_PI / 2.0;
		if (type == Block::Type::TRIANGLE) {
			rotation = 0;
		}
		auto tmp = std::make_unique<Block>(type, world);
		tmp->setRotation(rotation);
		return tmp;
	}
	if (x == 0) {
		return std::make_unique<Cannon>(world, cursor->getPlayerNr());
	}
	if (x == 1) {
		auto type = Block::Type::BALL;
		return std::make_unique<Block>(type, world);
	}
	if (x == 2) {
		return std::make_unique<MyPrecious>(world, true);
	}
	return std::make_unique<MyPrecious>(world, false);
}

void BuildMenu::draw() const {
	jngl::pushMatrix();
	jngl::translate(translate_vec);
	jngl::scale(fadeIn);
	for (auto& block : objects) {
		block->draw(false, 0.6);
		if (block->getPrice() <= cursor->getMoney()) {
			jngl::setFontColor(0, 255, 0);
		} else {
			jngl::setFontColor(255, 0, 0);
		}
		std::string text = std::to_string(block->getPrice()) + "€";
		jngl::print(text,
		            block->getPosition().x + 140.0 - jngl::getTextWidth(text),
		            block->getPosition().y - 20);
	}
	jngl::popMatrix();
}

jngl::Vec2 BuildMenu::translate_menu() const {
	return translate_vec + jngl::Vec2{ (double)x, (double)y };
}
