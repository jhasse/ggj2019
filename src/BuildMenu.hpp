#pragma once

#include <memory>
#include <jngl.hpp>
#include <vector>


class b2World;
class Block;
class Control;
class GameObject;
class Cursor;

class BuildMenu {
public:
	BuildMenu(b2World&, jngl::Vec2, Cursor* cursor);
	~BuildMenu();
	void step(Control&);
	void draw() const;
	jngl::Vec2 getSelectedPosition() const;

	/// Erstellt das aktuell ausgewählte Objekt
	std::unique_ptr<GameObject> createObject() const;

private:
	std::unique_ptr<GameObject> createObjectAt(int x, int y) const;
	jngl::Vec2 translate_menu() const;

	int x = 0;
	int y = 0;
	constexpr static int WIDTH = 4;
	constexpr static int HEIGHT = 2;

	bool waitForReleaseX = false;
	bool waitForReleaseY = false;

	float fadeIn = 0;

	std::vector<std::unique_ptr<GameObject>> objects;
	jngl::Vec2 translate_vec;
	b2World& world;

	Cursor* cursor;
};
