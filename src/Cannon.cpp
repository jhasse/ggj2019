#include "Cannon.hpp"
#include "CannonBall.hpp"

#include "constants.hpp"

#include <box2d/box2d.h>

Cannon::Cannon(b2World& world, int playerNr) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
	this->playerNr = playerNr;
}

Cannon::~Cannon() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool Cannon::step() {
	for (b2ContactEdge* edge = body->GetContactList(); edge; edge = edge->next) {
		if (!edge->contact->IsEnabled() || !edge->contact->IsTouching()) {
			continue;
		}
	}
	return false;
}

void Cannon::draw(const bool transparent, const float scale) const {
	jngl::pushSpriteAlpha(transparent ? 100 : 255);
	sprite.draw(jngl::modelview()
	                .translate(getPosition())
	                .rotate(getRotation())
	                .scale(playerNr == 0 ? -scale : scale, scale));
	jngl::popSpriteAlpha();
}

void Cannon::createFixture() {
	b2PolygonShape shape;
	std::vector<b2Vec2> points{
		pixelToMeter({ 2, 111 }),
		// pixelToMeter({ 19, 77 }),
		pixelToMeter({ 32, 64 }),
		pixelToMeter({ 33, 12}),
		pixelToMeter({ 54, 23 }),
		pixelToMeter({ 116, 6 }),
		pixelToMeter({ 126, 62 }),
		pixelToMeter({ 80, 87 }),
		pixelToMeter({ 100, 114 }),
	};
	for (auto& point : points) {
		point.x = point.x - 66. / PIXEL_PER_METER;
		if (playerNr == 0) {
			point.x = -point.x;
		}
		point.y = point.y - 60.6 / PIXEL_PER_METER;
	}
	shape.Set(&points[0], points.size());
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits =
	    (playerNr == 0) ? FILTER_CATEGORY_CANON_PLAYER0 : FILTER_CATEGORY_CANON_PLAYER1;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
	body->ApplyLinearImpulseToCenter({ 0, -2 * body->GetMass() }, true);
}

int Cannon::getPrice() const {
	return 400;
}

std::shared_ptr<GameObject> Cannon::fire() {
	jngl::play("sfx/shot.ogg");
	// std::shared_ptr<GameObject> newObject = CannonBall(b2World);
	// body->GetTransform().q.GetAngle()
	float speed = 10;
	// 45 °
	float start_angle = (M_PI / 180) * 30;
	if(playerNr == 0) {
		// flip
		start_angle *= -1;
		start_angle += M_PI;
	}
	jngl::Vec2 v = { cos(body->GetTransform().q.GetAngle() - start_angle) * speed,
		             sin(body->GetTransform().q.GetAngle() - start_angle) * speed };
	return std::make_shared<CannonBall>(*body->GetWorld(), getPosition(), v, playerNr);
}
