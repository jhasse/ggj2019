#pragma once

#include "GameObject.hpp"
#include <jngl.hpp>

class b2World;

class Cannon : public GameObject {
public:
	Cannon(b2World&, int playerNr);
	~Cannon();
	bool step() override;
	void draw(bool transparent, float scale) const override;
	void createFixture() override;
	int getPrice() const override;

	std::shared_ptr<GameObject> fire();

private:
	jngl::Sprite sprite{ "gfx/canon" };
	int playerNr;
};
