#include "CannonBall.hpp"

#include "constants.hpp"

#include <box2d/box2d.h>

CannonBall::CannonBall(b2World& world, const jngl::Vec2 position, jngl::Vec2 velocity,
                       const int playerNr)
: red(rand() % 0xff + 1), green(rand() % 0xff + 1), blue(rand() % 0xff + 1), playerNr(playerNr) {
	vel = velocity;
	b2BodyDef bodyDef;
	bodyDef.position = pixelToMeter(position);
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
	live = 200;
}

CannonBall::~CannonBall() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool CannonBall::step() {
	live = live - 1;
	if (live < 0) {
		return true;
	}
	for (b2ContactEdge* edge = body->GetContactList(); edge; edge = edge->next) {
		if (!edge->contact->IsEnabled() || !edge->contact->IsTouching()) {
			continue;
		}
	}
	return false;
}

void CannonBall::draw(const bool transparent, float scale) const {
	jngl::setSpriteColor(red, green, blue);
	sprite.draw(jngl::modelview().translate(getPosition()).scale(scale));
	jngl::setSpriteColor(0xff, 0xff, 0xff, 0xff);
}

void CannonBall::createFixture() {
	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 20 / PIXEL_PER_METER;
	createFixtureFromShape(shape);
}

void CannonBall::createFixtureFromShape(const b2CircleShape& shape) {
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 10.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	if (playerNr == 0) {
		fixtureDef.filter.maskBits = 0xffff & ~FILTER_CATEGORY_CANON_PLAYER0;
	} else {
		fixtureDef.filter.maskBits = 0xffff & ~FILTER_CATEGORY_CANON_PLAYER1;
	}
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
	body->ApplyLinearImpulseToCenter({ 0, -2 * body->GetMass() }, true);
	body->SetLinearVelocity({ static_cast<float>(vel.x), static_cast<float>(vel.y) });
}

int CannonBall::getPrice() const {
	return 0;
}
