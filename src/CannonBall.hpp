#pragma once

#include "GameObject.hpp"

#include <jngl.hpp>

class b2World;
class b2CircleShape;

class CannonBall : public GameObject {
public:
	CannonBall(b2World&, jngl::Vec2, jngl::Vec2, int playerNr);
	~CannonBall();
	bool step() override;
	void draw(bool transparent, float scale) const override;
	void createFixture() override;
	int getPrice() const override;
	void createFixtureFromShape(const b2CircleShape& shape);

private:
	jngl::Vec2 vel;
	unsigned char red;
	unsigned char green;
	unsigned char blue;
	int live;
	const int playerNr;
	jngl::Sprite sprite{ "gfx/canon_ball" };
};
