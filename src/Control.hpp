#pragma once

#include <jngl/Vec2.hpp>

class Control {
public:
	virtual ~Control() = default;

	/// Richtungsvektor
	virtual jngl::Vec2 getMovement() const = 0;

	/// Auswählen oder Spawnen
	virtual bool getPrimary() const = 0;

	/// Zum Bau-Menü wechseln
	virtual bool getSecondary() const = 0;

	virtual bool getRotateLeft() const = 0;
	virtual bool getRotateRight() const = 0;
};
