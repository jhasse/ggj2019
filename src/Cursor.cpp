#include "Cursor.hpp"

#include "constants.hpp"
#include "Game.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"
#include "MyPrecious.hpp"
#include "engine/Text.hpp"
#include "engine/effects/FadeOut.hpp"

#include <sstream>

Cursor::Cursor(b2World& world, const int playerNr)
: playerNr(playerNr),
  buildMenu(world, jngl::Vec2({ ((playerNr == 0) ? 1 : -1) * BOUNDS_W / 2.0, 465 }), this) {
	const auto controllers = jngl::getConnectedControllers();
	if (controllers.size() > playerNr) {
		control = std::make_unique<Gamepad>(controllers[playerNr], playerNr);
	} else {
		control = std::make_unique<Keyboard>(playerNr);
	}
	position.x += ((playerNr == 0) ? 1 : -1) * BOUNDS_W / 2.0;
	last_position = position;

	std::string sprite_src("gfx/player_blue");
	if (playerNr == 1) {
		sprite_src = std::string("gfx/player_red");
	}
	sprite = std::make_unique<jngl::Sprite>(sprite_src);

	loseText.setCenter(((playerNr == 0) ? 1 : -1) * BOUNDS_W / 2., 0);
	sprite->setCenter(last_position = buildMenu.getSelectedPosition());
}

Cursor::~Cursor() = default;

void Cursor::step(Game& game) {
	if (firstStep) {
		firstStep = false;
		auto firstTreasure = std::make_shared<MyPrecious>(game.getWorld(), false);
		firstTreasure->setPosition({ ((playerNr == 0) ? 1 : -1) * BOUNDS_W / 2.0, 100 });
		myTreasures.emplace_back(firstTreasure);
		game.spawn(firstTreasure);
	}
	if (lost) {
		return;
	}
	if (--moneyCountdown == 0) {
		for (auto it = myTreasures.begin(); it != myTreasures.end();) {
			if (it->expired()) {
				it = myTreasures.erase(it);
			} else {
				++it;
			}
		}

		for (const auto& treasure : myTreasures) {
			std::stringstream sstream;
			sstream << "+" << treasure.lock()->getAmount();
			const auto text = new Text(sstream.str(), treasure.lock()->getPosition());
			text->addEffect(std::make_unique<FadeOut>());
			text->add();
			money += treasure.lock()->getAmount();
		}
		moneyCountdown = 120;
		if (myTreasures.empty()) {
			lost = true;
		}
	}
	if (control->getSecondary()) {
		buildMenuActive = !buildMenuActive;
		if (buildMenuActive) {
			last_position = position;
		} else {
			last_position = buildMenu.getSelectedPosition();
		}
	}
	if (buildMenuActive) {
		fadeSpeed = 0.2;
		buildMenu.step(*control);
		last_position += (buildMenu.getSelectedPosition() - last_position) * fadeSpeed;
		sprite->setCenter(last_position);

		if (control->getPrimary()) {
			buildMenuActive = false;
			last_position = buildMenu.getSelectedPosition();
		} else {
			object.reset();
			return;
		}
	} else {
		fadeSpeed += (1 - fadeSpeed) * 0.03;
	}
	int factor = 1;
	if (playerNr == 1) {
		factor = -1;
	}
	jngl::Vec2 delta = control->getMovement() * 5;

	if ((position.x + delta.x) * factor > BOUNDS_W) {
		position.x = BOUNDS_W * factor;
	} else if ((position.x + delta.x) * factor < 0) {
		position.x = 0;
	} else {
		position.x += delta.x;
	}

	if ((position.y + delta.y) > GROUND) {
		position.y = GROUND;
	} else if ((position.y + delta.y) < -BOUNDS_H) {
		position.y = -BOUNDS_H;
	} else {
		position.y += delta.y;
	}
	if (object) {
		object->setPosition(position);
		if (control->getRotateLeft()) {
			object->setRotation(object->getRotation() - M_PI / 4);
		}
		if (control->getRotateRight()) {
			object->setRotation(object->getRotation() + M_PI / 4);
		}
		if (control->getPrimary()) {
			if (money >= object->getPrice()) {
				money -= object->getPrice();
				const float rotation = object->getRotation();
				std::shared_ptr<GameObject> newObject = std::move(object);
				game.spawn(newObject);
				jngl::play("sfx/plop.ogg");
				if (std::dynamic_pointer_cast<MyPrecious>(newObject)) {
					myTreasures.emplace_back(newObject);
				}
				object = buildMenu.createObject();
				object->setPosition(position);
				object->setRotation(rotation);
			}
		}
	} else {
		object = buildMenu.createObject();
		object->setPosition(position);
	}

	last_position += (position - last_position) * fadeSpeed;
	sprite->setCenter(last_position);
}

void Cursor::draw() const {
	if (!buildMenuActive) {
		if (object && !lost) {
			if (money < object->getPrice()) {
				jngl::setSpriteColor(255, 0, 0);
			}
			// Da fadeSpeed langsam 1 wird, nachdem man im Menü was ausgwählt hat, blenden wir
			// hiermit das Objekt langsam ein:
			jngl::setSpriteAlpha(fadeSpeed * fadeSpeed * 255);

			object->draw(true, 1);
			jngl::setSpriteColor(255, 255, 255, 255);
		}
	}
	if (lost) {
		jngl::setFontColor(0, 0, 0, 255);
		loseText.draw();
	}

	buildMenu.draw();
	if (!lost) {
		sprite->draw();
	}
	draw_money();
}

void Cursor::draw_money() const {
	float money_box_h = 40;
	float money_box_w = 150;
	float x = -BOUNDS_W;
	float y = -BOUNDS_H;
	float border_size = 4;
	int font_size = 30;
	std::string text = std::to_string(money) + "€";
	if (playerNr == 0) {
		x = (x * -1) - money_box_w;
	}

	jngl::setColor(0xffffff_rgb, 200);
	jngl::drawRect({ x, y }, { money_box_w, money_box_h });
	jngl::setColor(0xffff77_rgb, 255);
	jngl::drawRect({ x + border_size, y + border_size },
	               { money_box_w - 2 * border_size, money_box_h - 2 * border_size });
	jngl::setFontSize(font_size);
	jngl::setFontColor(0, 0, 0, 255);

	// align right
	jngl::print(text, x - border_size + money_box_w - jngl::getTextWidth(text), y + border_size);
}

int Cursor::getPlayerNr() const {
	return playerNr;
}

int Cursor::getMoney() const {
	return money;
}

bool Cursor::hasLost() const {
	return lost;
}
