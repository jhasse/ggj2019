#pragma once

#include "BuildMenu.hpp"

#include <jngl.hpp>

class Block;
class Control;
class Game;

class Cursor {
public:
	explicit Cursor(b2World& world, int playerNr);
	~Cursor();

	void step(Game&);
	void draw() const;
	int getPlayerNr() const;
	int getMoney() const;
	bool hasLost() const;

private:
	jngl::Vec2 position;
	int playerNr;
	std::unique_ptr<Control> control;
	std::unique_ptr<GameObject> object;
	BuildMenu buildMenu;
	bool buildMenuActive = true;
	std::unique_ptr<jngl::Sprite> sprite;
	jngl::Vec2 last_position;

	/// Wenn gegen 0, sehr langsame Animation, wenn 1, gar keine Animation
	float fadeSpeed = 1;
#ifdef NDEBUG
	int money = 100;
#else
	int money = 9990;
#endif

	/// Wenn 0, gibt's nen Euro umsonst,
	int moneyCountdown = 120;

	void draw_money() const;

	/// Alle Kisten, die mir gehören
	std::vector<std::weak_ptr<GameObject>> myTreasures;

	bool firstStep = true;
	bool lost = false;
	jngl::Text loseText{ "YOU LOSE!" };
};
