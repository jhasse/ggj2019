#include "Game.hpp"

#include "Cannon.hpp"
#include "constants.hpp"
#include "engine/Animation.hpp"
#include "engine/Fade.hpp"
#include "engine/Sprite.hpp"
#include "MyPrecious.hpp"
#include "Raindrop.hpp"

#include <cmath>

Game::Game(int scorePlayer0, int scorePlayer1)
: world({ 0, 9.8 /* gravity */ }), cursor0(world, 0), cursor1(world, 1),
  groundPosition(pixelToMeter({ 0, GROUND + PIXEL_PER_METER })),
  incomingText("EARTHQUAKE INCOMING"), scorePlayer0(scorePlayer0), scorePlayer1(scorePlayer1),
  scoreText0(std::to_string(scorePlayer0)), scoreText1(std::to_string(scorePlayer1)) {

	static auto font = std::make_shared<jngl::Font>("Lato-Regular.ttf", 50);
	incomingText.setFont(*font);
	incomingText.setCenter(jngl::Vec2(0, -300));

	scoreText0.setFont(*font);
	scoreText0.setCenter(80, -480);
	scoreText1.setFont(*font);
	scoreText1.setCenter(-80, -480);
	colon.setFont(*font);
	colon.setCenter(0, -484);

	b2BodyDef bodyDef;
	bodyDef.position = groundPosition;
	bodyDef.type = b2_kinematicBody;
	ground = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(BOUNDS_W / PIXEL_PER_METER, 1);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	ground->CreateFixture(&fixtureDef);
}

Game::~Game() = default;

void Game::onLoad() {
	jngl::setMouseVisible(false);
}

void Game::step() {
	if (rainIntensity < 9) {
		rainIntensity += 0.003;
	} else {
		rainIntensity = -8;
	}
	if (earthQuakeCountdown > 0) {
		--earthQuakeCountdown;
		if (earthQuakeCountdown == 0) {
			earthQuakeCountdown = -2000;
			ground->SetLinearVelocity(b2Vec2(0, 0));
			ground->SetTransform(groundPosition, 0);
			earthQuakeIntensity += 0.2f; // Nächstes wird stärker
		} else {
			ground->SetLinearVelocity(b2Vec2((rand() % 80 - 40) / 80. * earthQuakeIntensity,
			                                 (rand() % 80 - 40) / 80. * earthQuakeIntensity));
			if (earthQuakeCountdown % 20 == 0) {
				ground->SetTransform(groundPosition, 0);
			}
		}
	} else {
		++earthQuakeCountdown;
		if (earthQuakeCountdown == 0) {
			earthQuakeCountdown = 500;
			jngl::play("sfx/earthquake.ogg");
		}
	}
	world.Step(1.0f / 60.0f, 8, 3);

	cursor0.step(*this);
	cursor1.step(*this);

	for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
		// test fire
		if (auto cannon = std::dynamic_pointer_cast<Cannon>(*it)) {
			if (--cannon_countdown == 0) {
				std::shared_ptr<CannonBall> cannon_ball =
				    std::dynamic_pointer_cast<CannonBall>(cannon->fire());
				cannon_ball->createFixture();
				cannon_balls.emplace_back(std::move(cannon_ball));
				cannon_countdown = 179;
			}
		}
		if ((*it)->step() || (*it)->getPosition().y > 1000) {
			if (std::dynamic_pointer_cast<MyPrecious>(*it)) {
				jngl::play("sfx/scream.ogg");
				animations.emplace_back(std::make_unique<Animation>("explosion", 1, false));
				animations.back()->setPosition((*it)->getPosition());
			}
			it = gameObjects.erase(it);
			if (it == gameObjects.end()) {
				break;
			}
		}
	}
	for (auto it = animations.begin(); it != animations.end(); ++it) {
		if ((*it)->step()) {
			it = animations.erase(it);
			if (it == animations.end()) {
				break;
			}
		}
	}

	for (auto it = cannon_balls.begin(); it != cannon_balls.end(); ++it) {
		if ((*it)->step()) {
			it = cannon_balls.erase(it);
			if (it == cannon_balls.end()) {
				break;
			}
		}
	}

	for (auto it = raindrops.begin(); it != raindrops.end(); ++it) {
		if ((*it)->step()) {
			it = raindrops.erase(it);
			if (it == raindrops.end()) {
				break;
			}
		}
	}

	const int absRainIntensity = std::abs(std::lround(rainIntensity)) - 2;
	if (absRainIntensity > 0) {
		if (!jngl::isPlaying("sfx/rain.ogg")) {
			jngl::play("sfx/rain.ogg");
		}
	} else {
		jngl::stop("sfx/rain.ogg");
	}
	for (int i = 0; i < absRainIntensity ; ++i) {
		raindrops.emplace_back(std::make_unique<Raindrop>(
		    world, jngl::Vec2(rand() % (BOUNDS_W * 2) - BOUNDS_W, -BOUNDS_H * 1.01 - rand() % 50)));
	}
	for (auto& sprite : sprites) {
		sprite->step();
	}
	for (auto& sprite : needToRemove) {
		sprites.erase(std::remove_if(
		                  sprites.begin(), sprites.end(),
		                  [sprite](const std::unique_ptr<Sprite>& p) { return p.get() == sprite; }),
		              sprites.end());
	}
	needToRemove.clear();

	if (gameoverCountdown == -1 && (cursor0.hasLost() || cursor1.hasLost())) {
		gameoverCountdown = 300;
	}
	if (gameoverCountdown > 0 && --gameoverCountdown == 0) {
		const int newScore0 = scorePlayer0 + (cursor1.hasLost() ? 1 : 0);
		const int newScore1 = scorePlayer1 + (cursor0.hasLost() ? 1 : 0);
		jngl::setWork(std::make_shared<Fade>(std::make_shared<Game>(newScore0, newScore1)));
	}
	if (earthQuakeCountdown == -300) {
		jngl::play("sfx/siren.ogg");
	}
}

void Game::draw() const {
	jngl::setBackgroundColor(0x3c86e6_rgb);
	jngl::draw("gfx/game", -960, -540);

	for (auto& gameObject : gameObjects) {
		gameObject->draw(false, 1);
	}
	for (auto& animation : animations) {
		animation->draw();
	}
	for (auto& raindrop : raindrops) {
		raindrop->draw();
	}
	for (auto& sprite : sprites) {
		sprite->draw();
	}
	for (auto& cannon_ball : cannon_balls) {
		cannon_ball->draw(false, 1);
	}

	jngl::pushMatrix();
	if (earthQuakeCountdown > 0) {
		b2Vec2 dif = ground->GetPosition() - groundPosition;
		jngl::translate(dif.x * 200, dif.y * 200);
	}
	jngl::setFontColor(20, 20, 20, 255);
	scoreText0.draw();
	scoreText1.draw();
	colon.draw();
	cursor0.draw();
	cursor1.draw();
	jngl::popMatrix();


	if (earthQuakeCountdown < 0 && -earthQuakeCountdown < 300 && -earthQuakeCountdown % 60 < 30) {
		jngl::setFontColor(255, 10, 10, 200);
		incomingText.draw();
	}
}

void Game::spawn(std::shared_ptr<GameObject> gameObject) {
	gameObjects.emplace_back(std::move(gameObject));
	gameObjects.back()->createFixture();
}

b2World& Game::getWorld() {
	return world;
}

void Game::addSprite(Sprite* sprite) {
	sprites.emplace_back(sprite);
}

void Game::removeSprite(Sprite* sprite) {
	needToRemove.insert(sprite);
}
