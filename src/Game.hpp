#pragma once

#include "Cursor.hpp"
#include "CannonBall.hpp"

#include <box2d/box2d.h>
#include <set>

class Animation;
class GameObject;
class Raindrop;
class Sprite;

class Game : public jngl::Work {
public:
	Game(int scorePlayer0, int scorePlayer1);
	~Game();
	void onLoad() override;
	void step() override;
	void draw() const override;
	void spawn(std::shared_ptr<GameObject>);
	void addSprite(Sprite*);
	void removeSprite(Sprite*);

	/// Böse!
	b2World& getWorld();

private:
	b2World world;

	/// Die Cursor der beiden Spieler, mit denen sie Sachen auswählen können
	Cursor cursor0;
	Cursor cursor1;

	/// Alle gebauten Blöcke
	std::vector<std::shared_ptr<GameObject>> gameObjects;

	std::vector<std::unique_ptr<Animation>> animations;
	std::vector<std::unique_ptr<Raindrop>> raindrops;

	std::vector<std::shared_ptr<CannonBall>> cannon_balls;
	int cannon_countdown = 180;

	// Über 10 fängt es an zu ruckeln
	float rainIntensity = -2;

	std::vector<std::unique_ptr<Sprite>> sprites;
	std::set<Sprite*> needToRemove;

	b2Body* ground;
	const b2Vec2 groundPosition;

	/// Solange größer 0, wackelt die Erde
	int earthQuakeCountdown = -4000;

	float earthQuakeIntensity = 0.8f;

	jngl::Text incomingText;

	const int scorePlayer0;
	const int scorePlayer1;

	jngl::Text colon{":"};
	jngl::Text scoreText0;
	jngl::Text scoreText1;

	int gameoverCountdown = -1;
};
