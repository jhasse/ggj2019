#include "Keyboard.hpp"

#include <jngl.hpp>

Keyboard::Keyboard(int playerNr) : playerNr(playerNr) {
}

jngl::Vec2 Keyboard::getMovement() const {
	jngl::Vec2 direction;
	if (playerNr == 0) {
		if (jngl::keyDown(jngl::key::Left)) {
			direction -= jngl::Vec2(1, 0);
		}
		if (jngl::keyDown(jngl::key::Right)) {
			direction += jngl::Vec2(1, 0);
		}
		if (jngl::keyDown(jngl::key::Up)) {
			direction -= jngl::Vec2(0, 1);
		}
		if (jngl::keyDown(jngl::key::Down)) {
			direction += jngl::Vec2(0, 1);
		}
	} else {
		if (jngl::keyDown('a')) {
			direction -= jngl::Vec2(1, 0);
		}
		if (jngl::keyDown('d')) {
			direction += jngl::Vec2(1, 0);
		}
		if (jngl::keyDown('w')) {
			direction -= jngl::Vec2(0, 1);
		}
		if (jngl::keyDown('s')) {
			direction += jngl::Vec2(0, 1);
		}
	}
	if (boost::qvm::mag_sqr(direction) > 1) {
		boost::qvm::normalize(direction);
	}
	return direction;
}

bool Keyboard::getPrimary() const {
	if (playerNr == 0) {
		return jngl::keyPressed(jngl::key::ControlR) || jngl::keyPressed(jngl::key::AltR);
	}
	return jngl::keyPressed(jngl::key::ControlL);
}

bool Keyboard::getSecondary() const {
	if (playerNr == 0) {
		return jngl::keyPressed('m');
	}
	return jngl::keyPressed(jngl::key::ShiftL);
}

bool Keyboard::getRotateLeft() const {
	if (playerNr == 0) {
		return jngl::keyPressed(',');
	}
	return jngl::keyPressed('q');
}

bool Keyboard::getRotateRight() const {
	if (playerNr == 0) {
		return jngl::keyPressed('.');
	}
	return jngl::keyPressed('e');
}
