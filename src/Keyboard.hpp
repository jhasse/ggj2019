#pragma once

#include "Control.hpp"

class Keyboard : public Control {
public:
	Keyboard(int playerNr);

	jngl::Vec2 getMovement() const override;
	bool getPrimary() const override;
	bool getSecondary() const override;
	bool getRotateLeft() const override;
	bool getRotateRight() const override;

private:
	const int playerNr;
};
