#include "MyPrecious.hpp"

#include "constants.hpp"
#include "BasicObject.hpp"

#include <box2d/box2d.h>

MyPrecious::MyPrecious(b2World& world, bool small)
: small(small), sprite{ small ? "gfx/dude" : "gfx/family" } {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
}

MyPrecious::~MyPrecious() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool MyPrecious::step() {
	if (!innerBox) { return false; }
	if (lifetime < 100) {
		++lifetime;
	} else {
		for (b2ContactEdge* edge = body->GetContactList(); edge; edge = edge->next) {
			if (!edge->contact->IsEnabled() || !edge->contact->IsTouching()) {
				continue;
			}
			const auto other = reinterpret_cast<BasicObject*>(edge->other->GetUserData().pointer);
			if ((other && other->destroysTreasure()) ||
			    (edge->contact->GetFixtureA() == innerBox ||
			     edge->contact->GetFixtureB() == innerBox)) {
				++damage;
				if (damage == MAX_DAMAGE) {
					return true;
				}
			}
		}
	}
	return false;
}

void MyPrecious::draw(const bool transparent, const float scale) const {
	jngl::pushSpriteAlpha(transparent ? 100 : 255);
	jngl::setSpriteColor(255 - damage * 20, 255 - damage * 20, 255 - damage * 20);
	sprite.draw(jngl::modelview().translate(getPosition()).rotate(getRotation()).scale(scale));
	jngl::setSpriteColor(255, 255, 255);
	jngl::popSpriteAlpha();
}

void MyPrecious::createFixture() {
	b2PolygonShape shape;
	if (small) {
		shape.SetAsBox(20. / PIXEL_PER_METER / 2., 50. / PIXEL_PER_METER / 2.);
		createFixtureFromShape(shape);
		shape.SetAsBox(12. / PIXEL_PER_METER / 2., 42. / PIXEL_PER_METER / 2.);
	} else {
		shape.SetAsBox(60. / PIXEL_PER_METER / 2., 60. / PIXEL_PER_METER / 2.);
		createFixtureFromShape(shape);
		shape.SetAsBox(52. / PIXEL_PER_METER / 2., 52. / PIXEL_PER_METER / 2.);
	}
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	innerBox = body->CreateFixture(&fixtureDef);
}

int MyPrecious::getPrice() const {
	if (small) {
		return 50;
	}
	return 100;
}

int MyPrecious::getAmount() const {
	return small ? 2 : 5;
}
