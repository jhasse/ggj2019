#pragma once

#include "GameObject.hpp"

#include <jngl.hpp>

class b2Fixture;
class b2World;

/// Objekt, dass man vorm Regen schützen muss
class MyPrecious : public GameObject {
public:
	MyPrecious(b2World&, bool small);
	~MyPrecious();
	bool step() override;
	void draw(bool transparent, float scale) const override;
	void createFixture() override;
	int getPrice() const override;
	int getAmount() const override;

private:
	const bool small;
	jngl::Sprite sprite;
	b2Fixture* innerBox = nullptr;

	/// Wie viele Frames wir schon eine Fixture haben
	int lifetime = 0;

	constexpr static int MAX_DAMAGE = 10;
	int damage = 0;
};
