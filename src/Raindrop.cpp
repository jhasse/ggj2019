#include "Raindrop.hpp"

#include "constants.hpp"

#include <box2d/box2d.h>
#include <cmath>
#include <jngl.hpp>

Raindrop::Raindrop(b2World& world, const jngl::Vec2 position) : world(world) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = pixelToMeter(position);
	body = world.CreateBody(&bodyDef);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<BasicObject*>(this));

	b2CircleShape shape;
	// Nochmal durch 4, damit die Regentropfen nicht zu sehr beim Bauen stören:
	shape.m_radius = RADIUS / PIXEL_PER_METER / 4.0;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_NON_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	fixtureDef.filter.groupIndex = -1; // negativ heißt: nicht mit sich selber kollidieren
	body->CreateFixture(&fixtureDef);
	body->SetBullet(true);

	// Direkt anfangen zu fallen:
	body->SetLinearVelocity({0, 12});
}

Raindrop::~Raindrop() {
	world.DestroyBody(body);
}

bool Raindrop::step() {
	if (countdown > 0) {
		--countdown;
		return countdown == 0;
	}
	if (body->GetContactList()) {
		countdown = MAX_COUNTDOWN;
	}
	return false;
}

void Raindrop::draw() const {
	jngl::setColor(0x68dada_rgb, 255);
	float alphaFactor = float(countdown) / float(MAX_COUNTDOWN);
	if (countdown > 0) {
		jngl::setAlpha(static_cast<unsigned char>(alphaFactor * 255));
		if (countdown != MAX_COUNTDOWN) {
			jngl::drawCircle(meterToPixel(body->GetPosition()), RADIUS);
		}
	} else {
		alphaFactor = 1;
	}
	const jngl::Vec2 tmp(RADIUS, RADIUS * 10 * std::pow(alphaFactor, 50));
	jngl::drawRect(meterToPixel(body->GetPosition()) - jngl::Vec2(tmp.x / 2., tmp.y / 2), tmp);
}

bool Raindrop::destroysTreasure() const {
	return true;
}
