#pragma once

#include "BasicObject.hpp"

#include <jngl/Vec2.hpp>

class b2Body;
class b2World;

class Raindrop : public BasicObject {
public:
	Raindrop(b2World&, jngl::Vec2 position);

	~Raindrop();

	/// Gibt true zurück, wenn dieser Regentropfen gelöscht werden soll
	bool step();

	void draw() const;

	bool destroysTreasure() const override;

private:
	constexpr static float RADIUS = 2;
	constexpr static int MAX_COUNTDOWN = 28;

	b2Body* body = nullptr;
	b2World& world;

	// Wenn >0 zählen wir bis 0 und zerstören uns dann
	int countdown = -1;
};
