#include "Paths.hpp"

#include "../constants.hpp"

#include <jngl.hpp>

Paths::Paths() = default;

std::string Paths::Graphics() {
	return graphics_;
}

std::string Paths::fonts() const {
	return "fonts/";
}

void Paths::setGraphics(const std::string& graphics) {
	graphics_ = graphics;
}

Paths& getPaths() {
	return *Paths::handle();
}

std::string Paths::OriginalGfx() const {
	return originalGfx_;
}

void Paths::setOriginalGfx(const std::string& originalGfx) {
	originalGfx_ = originalGfx;
}
