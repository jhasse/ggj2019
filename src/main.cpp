#include "constants.hpp"
#include "Intro.hpp"

#include <jngl/init.hpp>

jngl::AppParameters jnglInit() {
	jngl::AppParameters params;
	params.displayName = programDisplayName;
	params.screenSize = { BOUNDS_W, BOUNDS_H };
	params.start = []() {
		jngl::setAntiAliasing(true);
		return std::make_shared<Intro>();
	};
	return params;
}
